import mysql.connector
from datetime import datetime

def sendlastlinetodb():
    # Connect to your MySQL database
    conn = mysql.connector.connect(
        host='your-host-name', #to replace
        user='your-user-name', #to replace
        password='your-password', #to replace
        database='your-database-name' #to replace
    )

    cursor = conn.cursor()

    # Read data from the file
    now = datetime.now()
    file_path = now.strftime("%Y%m%d") + "useful_data.txt"
    # file_path = "20231206useful_data.txt"

    # Read all lines from the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
        last_line = lines[-1]
    
        # Convert the string representation of a dictionary to an actual dictionary
        data_dict = eval(last_line)
        
        print("data_dict = ", data_dict)

        # Iterate through the sensors in the file
        for sensor_name, sensor_value in data_dict.items():
            # Search for the sensor in the capteur table
            cursor.execute("""
                SELECT capteur.id_capteur, parcelle.id_parcelle, parcelle.nom, jardin.id_jardin
                FROM capteur
                JOIN parcelle ON capteur.id_parcelle_fk = parcelle.id_parcelle
                JOIN jardin ON parcelle.id_jardin_fk = jardin.id_jardin
                WHERE capteur.nom = %s
            """, (sensor_name,))

            result = cursor.fetchone()
            
            print("result = ",result)

            if result:
                id_capteur, id_parcelle, nom_parcelle, id_jardin = result

                # Insert the data into the releve table
                cursor.execute("""
                    INSERT INTO releve (id_capteur_fk, valeur)
                    VALUES (%s, %s)
                """, (id_capteur, sensor_value))

                # Commit the changes
                conn.commit()

                print(f"Data inserted for sensor '{sensor_name}' in parcel '{nom_parcelle}' of jardin '{id_jardin}'.")
            else:
                print(f"Sensor '{sensor_name}' not found in the database.")

    # Close the connection
    conn.close()
    print("Execution complete!")

# Call the function to send data to the database
#sendlastlinetodb()
