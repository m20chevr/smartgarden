from network import LoRa
import socket
import time
import struct
import ubinascii
import binascii
import pycom

from machine import enable_irq, disable_irq, Pin
from dht import DTH
import machine 

###################################################################################################################
def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation),
    # choose dr = 0 if the gateway is not close to your device, try different dr if needed
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    # wait until the module has joined the network
    pycom.rgbled(0x7f7f00) #yellow
    while not lora_object.has_joined():
        time.sleep(2.5)
        print('Not yet joined...')
        lora.nvram_erase()
        
pycom.heartbeat(False)

app_eui = ubinascii.unhexlify('################')# replace the dash by the app_eui provided by TTN
app_key = ubinascii.unhexlify('#################') # replace the dash by the AppKey provided by TTN
#uncomment to use LoRaWAN application provided dev_eui
dev_eui = ubinascii.unhexlify('#################') # replace the dash by the DevEUI provided to TTN

pycom.rgbled(0xff0000) #red
time.sleep(1)

# configure the parameters of LoRaWAN, authorize adaptive datarate and choose a CLASS C device
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, device_class=LoRa.CLASS_C, adr=False)
print('DevEUI : ', binascii.hexlify(lora.mac()).upper())
lora.nvram_restore() #if there is nothing to restore it will return a False

connect_to_ttn(lora)

print("CONNECTED!!")
pycom.rgbled(0x00ff00) #green

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)
###################################################################################################################

#Mettre votre n° de broche
th = DTH('P8')
pycom.heartbeat(False)

#ADC capteur 
adc = machine.ADC() # create an ADC object
apin = adc.channel(pin='P13') # create an analog pin on P13 Funduino Moisture sensor 
apin2 = adc.channel(pin='P14') # create an analog pin on P14 DFROBOT Moisture sensor 

while True:
    result = th.read()
    while not result.is_valid():
        time.sleep(.5)
        result = th.read()

    print('Temperature:', result.temperature)

    print('Relativity Humidity:', result.humidity)

    val = apin() # read an analog value Funduino Moisture sensor 
    valp = int(val*(100/4096)) #int part (soulement 100 valeurs)
    print('Humidity floor 1:',valp,'%')

    val2 = apin2() # read an analog value DFROBOT Moisture sensor 
    valp2 = int(val2*(100/4096)) #int part (soulement 100 valeurs)
    print('Humidity floor 2:',valp2,'%')

    # si le donées < 32 le code affichage format HEXA
    # si le donées => 32 le code affichage format CHAR

    #Temperature
    temp = bytes([result.temperature]) #convert to char de données
    #print('Sending Temperature: ',temp) 
    #s.send(temp)

    #Humidity relativity
    RHumidity = bytes([result.humidity]) #convert to char de données
    #print('Sending Humidity Relativity: ',RHumidity)
    #s.send(RHumidity)

    #Humidity floor 1
    Humidity1 = bytes([valp]) #convert to char de données
    #print('Sending Humidity floor 1: ',Humidity1)
    #s.send(Humidity1)

    #Humidity floor 2
    Humidity2 = bytes([valp2]) #convert to char de données
    #print('Sending Humidity floor 2: ',Humidity2,'\n')
    #s.send(Humidity2)

    meteo = temp + RHumidity + Humidity1 + Humidity2
    print('Sending: ')
    print('Temperature: ',temp ,'Relativity Humidity : ',RHumidity ,'Humidity floor 1 : ',Humidity1 ,'Humidity floor 2 : ',Humidity2)
    s.send(meteo)
    print('\n')
