from fastapi import FastAPI, HTTPException, Depends, status
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from datetime import timedelta, date, datetime
from sqlalchemy.orm import Session
from sqlalchemy import desc, func
from typing import Annotated
from .database import engine, SessionLocal
from .models import *

app = FastAPI()
Base.metadata.create_all(bind=engine)

origins = [
    "http://web-host",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

db_dependency = Annotated[Session, Depends(get_db)]

def get_start_and_end_of_week(date: date):
    first_day = date - timedelta(days = date.weekday())
    last_day = first_day + timedelta(days = 6)
    return first_day, last_day


class JardinBase(BaseModel):
    nom : str
    description : str
    numero_adresse : int
    rue : str
    code_postal : int
    ville : str
    latitude : float
    longitude : float
    identifiant : str
    mot_de_passe : str

class ParcelleBase(BaseModel):
    nom : str
    description : str
    jardin_id : int

class ActionBase(BaseModel):
    nom : str
    description : str

class FicheBase(BaseModel):
    nom : str
    description : str
    selectionner : bool
    planter_debut : date
    planter_fin  : date
    recolte_debut : date
    recolte_fin  : date
    parcelle_id  : int

class TacheBase(BaseModel):
    nom : str
    fait : bool
    date_creation : date
    date_fait : date
    date_a_faire : date
    action_id : int
    fiche_id : int

class CapteurBase(BaseModel):
    nom : str
    description : str
    unite : str
    parcelle_id : int

class ReleveBase(BaseModel):
    valeur : float
    date_releve : datetime
    capteur_id : int

@app.post("/jardin/", tags=["Jardin"], status_code=status.HTTP_201_CREATED)
async def create_jardin(jardin: JardinBase, db: db_dependency):
    db_jardin = Jardin(**jardin.dict())
    db.add(db_jardin)
    db.commit()

@app.get("/jardin/{jardin_id}", tags=["Jardin"], status_code=status.HTTP_200_OK)
async def read_jardin(jardin_id: int, db: db_dependency):
    jardin = db.query(Jardin).filter(Jardin.id == jardin_id).first()
    if jardin is None:
        raise HTTPException(status_code=404, detail="Jardin not found")
    return jardin

@app.post("/parcelle/", tags=["Parcelle"], status_code=status.HTTP_201_CREATED)
async def create_parcelle(parcelle: ParcelleBase, db: db_dependency):
    db_parcelle = Parcelle(**parcelle.dict())
    db.add(db_parcelle)
    db.commit()

@app.get("/parcelle/{parcelle_id}", tags=["Parcelle"], status_code=status.HTTP_200_OK)
async def read_parcelle(parcelle_id: int, db: db_dependency):
    parcelle = db.query(Parcelle).filter(Parcelle.id == parcelle_id).first()
    if parcelle is None:
        raise HTTPException(status_code=404, detail="Parcelle not found")
    return parcelle

@app.post("/action/", tags=["Action"], status_code=status.HTTP_201_CREATED)
async def create_action(action: ActionBase, db: db_dependency):
    db_action = Action(**action.dict())
    db.add(db_action)
    db.commit()

@app.get("/action/{action_id}", tags=["Action"], status_code=status.HTTP_200_OK)
async def read_action(action_id: int, db: db_dependency):
    action = db.query(Action).filter(Action.id == action_id).first()
    if action is None:
        raise HTTPException(status_code=404, detail="Action not found")
    return action

@app.post("/fiche/", tags=["Fiche"], status_code=status.HTTP_201_CREATED)
async def create_fiche(fiche: FicheBase, db: db_dependency):
    db_fiche = Fiche(**fiche.dict())
    db.add(db_fiche)
    db.commit()

@app.get("/fiche/{fiche_id}", tags=["Fiche"], status_code=status.HTTP_200_OK)
async def read_fiche(fiche_id: int, db: db_dependency):
    fiche = db.query(Fiche).filter(Fiche.id == fiche_id).first()
    if fiche is None:
        raise HTTPException(status_code=404, detail="Fiche not found")
    return fiche

@app.get("/fiches/", tags=["Fiche"], status_code=status.HTTP_200_OK)
async def read_fiches_list(db: db_dependency):
    fiches = db.query(Fiche).all()
    if fiches is None:
        raise HTTPException(status_code=404, detail="Fiche not found")
    return fiches

@app.put("/fiche/{fiche_id}", tags=["Fiche"], status_code=status.HTTP_200_OK)
async def update_fiche(fiche_id: int, updated_fiche: dict, db: db_dependency):
    existing_fiche = db.query(Fiche).filter(Fiche.id == fiche_id).first()
    if existing_fiche is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Fiche not found")
    # Mise à jour des champs de la tâche
    for key, value in updated_fiche.items():
        setattr(existing_fiche, key, value)
    # Enregistrement des modifications dans la base de données
    db.commit()

@app.delete("/fiche/{fiche_id}", tags=["Fiche"], status_code=status.HTTP_200_OK)
async def delete_fiche(fiche_id: int, db: db_dependency):
    fiche = db.query(Fiche).filter(Fiche.id == fiche_id).first()
    if fiche is None:
        raise HTTPException(status_code=404, detail="Fiche not found")
    db.delete(fiche)
    db.commit()

@app.post("/tache/", tags=["Tâche"], status_code=status.HTTP_201_CREATED)
async def create_tache(tache: TacheBase, db: db_dependency):
    db_tache = Tache(**tache.dict())
    db.add(db_tache)
    db.commit()

@app.get("/taches/", tags=["Tâche"], status_code=status.HTTP_200_OK)
async def read_taches_list(db: db_dependency):
    taches = db.query(Tache).all()
    if taches is None:
        raise HTTPException(status_code=404, detail="Tache not found")
    return taches

@app.get("/taches/{tache_date_a_faire}", tags=["Tâche"], status_code=status.HTTP_200_OK)
async def read_taches_by_date_a_faire(tache_date_a_faire: date, db: db_dependency):
    first_day_of_week, last_day_of_week = get_start_and_end_of_week(tache_date_a_faire)
    taches = db.query(Tache).join(Fiche, Tache.fiche_id == Fiche.id).filter(Fiche.selectionner == 1, Tache.date_a_faire >= first_day_of_week, Tache.date_a_faire <= last_day_of_week).all()
    taches_sans_fiche = db.query(Tache).filter(Tache.fiche_id == 0, Tache.date_a_faire >= first_day_of_week, Tache.date_a_faire <= last_day_of_week).all()
    return taches + taches_sans_fiche

@app.get("/tache/{tache_id}", tags=["Tâche"], status_code=status.HTTP_200_OK)
async def read_tache(tache_id: int, db: db_dependency):
    tache = db.query(Tache).filter(Tache.id == tache_id).first()
    if tache is None:
        raise HTTPException(status_code=404, detail="Tache not found")
    return tache

@app.put("/tache/{tache_id}", tags=["Tâche"], status_code=status.HTTP_200_OK)
async def update_tache(tache_id: int, updated_tache: dict, db: db_dependency):
    existing_tache = db.query(Tache).filter(Tache.id == tache_id).first()
    if existing_tache is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Tache not found")
    # Mise à jour des champs de la tâche
    for key, value in updated_tache.items():
        setattr(existing_tache, key, value)
    # Enregistrement des modifications dans la base de données
    db.commit()

@app.delete("/tache/{tache_id}", tags=["Tâche"], status_code=status.HTTP_200_OK)
async def delete_tache(tache_id: int, db: db_dependency):
    tache = db.query(Tache).filter(Tache.id == tache_id).first()
    if tache is None:
        raise HTTPException(status_code=404, detail="Tache not found")
    db.delete(tache)
    db.commit()

@app.post("/capteur/", tags=["Capteur"], status_code=status.HTTP_201_CREATED)
async def create_capteur(capteur: CapteurBase, db: db_dependency):
    db_capteur = Capteur(**capteur.dict())
    db.add(db_capteur)
    db.commit()

@app.get("/capteur_id/{capteur_id}", tags=["Capteur"], status_code=status.HTTP_200_OK)
async def read_capteur_by_id(capteur_id: int, db: db_dependency):
    capteur = db.query(Capteur).filter(Capteur.id == capteur_id).first()
    if capteur is None:
        raise HTTPException(status_code=404, detail="Capteur not found")
    return capteur

@app.get("/capteur_nom/{capteur_nom}", tags=["Capteur"], status_code=status.HTTP_200_OK)
async def read_capteur_by_name(capteur_nom: str, db: db_dependency):
    capteur = db.query(Capteur).filter(Capteur.nom == capteur_nom).first()
    if capteur is None:
        raise HTTPException(status_code=404, detail="Capteur not found")
    return capteur

@app.get("/capteurs/", tags=["Capteur"], status_code=status.HTTP_200_OK)
async def read_capteurs(db: db_dependency):
    capteurs = db.query(Capteur).all()
    if capteurs is None:
        raise HTTPException(status_code=404, detail="Capteurs not found")
    return capteurs

@app.get("/capteurs-releve/", tags=["Capteur"], status_code=status.HTTP_200_OK)
async def read_capteurs_last_releve(db: db_dependency):
        subquery = (
            db.query(
                Releve.capteur_id,
                func.max(Releve.date_releve).label("max_date_releve")
            )
            .group_by(Releve.capteur_id)
            .subquery()
        )

        # Requête principale pour récupérer les derniers relevés pour chaque capteur
        query = (
            db.query(
                Capteur,
                subquery.c.max_date_releve.label("date_releve"),
                Releve.valeur
            )
            .join(subquery, Capteur.id == subquery.c.capteur_id)
            .join(Releve, (Releve.capteur_id == Capteur.id) & (Releve.date_releve == subquery.c.max_date_releve))
            .all()
        )
        if not query:
            raise HTTPException(status_code=404, detail="Relevé not found")

        results = [
            {
                "capteur_id": capteur.id,
                "nom": capteur.nom,
                "description": capteur.description,
                "unite": capteur.unite,
                "parcelle_id": capteur.parcelle_id,
                "date_releve": date_releve,
                "valeur": valeur
            }
            for capteur, date_releve, valeur in query
        ]

        return results


@app.post("/releve/", tags=["Relevé"], status_code=status.HTTP_201_CREATED)
async def create_releve(releve: ReleveBase, db: db_dependency):
    db_releve = Releve(**releve.dict())
    db.add(db_releve)
    db.commit()

@app.get("/releve/{releve_id}", tags=["Relevé"], status_code=status.HTTP_200_OK)
async def read_releve(releve_id: int, db: db_dependency):
    releve = db.query(  Releve).filter(Releve.id == releve_id).first()
    if releve is None:
        raise HTTPException(status_code=404, detail="Releve not found")
    return releve
