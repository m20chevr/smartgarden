from sqlalchemy import Column, String, Integer, Float, Boolean, Date, DateTime
from .database import Base

class Jardin(Base):
    __tablename__ = 'jardin'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    description = Column(String(100))
    numero_adresse = Column(Integer)
    rue = Column(String(50))
    code_postal = Column(Integer)
    ville = Column(String(50))
    latitude = Column(Float)
    longitude = Column(Float)
    identifiant = Column(String(50), unique=True)
    mot_de_passe = Column(String(50))

class Parcelle(Base):
    __tablename__ = 'parcelle'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    description = Column(String(100))
    jardin_id = Column(Integer)

class Action(Base):
    __tablename__ = 'action'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    description = Column(String(100))

class Fiche(Base):
    __tablename__ = 'fiche'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    description = Column(String(100))
    selectionner = Column(Boolean)
    planter_debut = Column(Date)
    planter_fin = Column(Date)
    recolte_debut = Column(Date)
    recolte_fin = Column(Date)
    parcelle_id = Column(Integer)

class Tache(Base):
    __tablename__ = 'tache'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    fait = Column(Boolean)
    date_creation = Column(Date)
    date_fait = Column(Date)
    date_a_faire = Column(Date)
    action_id = Column(Integer)
    fiche_id = Column(Integer)

class Capteur(Base):
    __tablename__ = 'capteur'

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50))
    description = Column(String(100))
    unite = Column(String(10))
    parcelle_id = Column(Integer)

class Releve(Base):
    __tablename__ = 'releve'

    id = Column(Integer, primary_key=True, index=True)
    valeur = Column(Float)
    date_releve = Column(DateTime)
    capteur_id = Column(Integer)