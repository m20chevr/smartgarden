# SmartGarden

## Description
Ce projet a pour but de créer un site web permettant à des utilisateurs de jardins partagés d'organiser les tâches au sein du jardin.
Pour plus d'informations sur les jardins partagés et le cadre du projet, vous pouvez lire nos articles sur l'état de l'art et l'enquête terrain que nous avons réalisé.

## Démarrage du projet

Copier-coller le fichier `.env.template` en `.env` puis changer les variables.

Dans le dossier `lopy`, modifier les 3 fichiers `lopy.py`, `insertdatatodb.py` et `main.py` en complétant les champs demandés(ctrl + F "replace").

Réaliser un petit `docker compose up -d` pour lancer le projet.

Le frontend devient accessible sur le port `3000`, le backend sur `9002` et PHPMyAdmin sur `9001`.

Créer votre database via PHPMyAdmin puis relancer le backend.

Pour éteindre le docker, `docker compose down` (et pas `docker compose down -v` sinon suppression des volumes)

## Auteurs
Chevrier Maelys, Aina Dirou, Kangoute Souleymane, Quevedo Cristian
