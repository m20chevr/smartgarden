import React from 'react';
import Home from './pages/Home.js';
import Taches from './pages/Taches.js';
import Fiches from './pages/Fiches.js';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './components/Header.js';
import './styles/App.css'


function App() {
  return (
    <div>
      <Router>
        <Header/>
        <Routes>
          <Route path='/' exact Component={Home} />
          <Route path="*" exact Component={Home} />
          <Route path='/taches' exact Component={Taches}/>
          <Route path='/fiches' exact Component={Fiches}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
