import React from 'react';
import { useState, useEffect } from 'react';
import Popup from '../components/Popup';
import Tache from '../components/Tache';
import '../styles/Form.css'

function Taches() {
  const [toAdd, setToAdd] = useState(false);
  const [currentDate, setCurrentDate] = useState(new Date());
  const [taches, setTaches] = useState([]);
  const [reloadCount, setReloadCount] = useState(0);

  useEffect(() => {
    const date = formatDate(currentDate);
		fetch(`http://localhost:9002/taches/${date}`,{type: "GET"})
		  .then(res => res.json())
		  .then(
			(result) => {
				setTaches(result);
			},
			(error) => {
				console.log('Erreur lors de la requête GET :', error);
			}
		  )
	  }, [currentDate, reloadCount, toAdd])
	
  
  function handleSubmit(e) {
		e.preventDefault();
    const formData = {
			nom: e.target['nom'].value,
			fait: false,
			date_creation: new Date(Date.now()).toISOString().split('T')[0],
      date_fait: e.target['date_a_faire'].value,
      date_a_faire : e.target['date_a_faire'].value,
			action_id: 0,
      fiche_id: 0
		};
		fetch('http://localhost:9002/tache/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(formData),
		})
    .then(res => res.json())
    .then(res => reload())
		.catch(error => {
			console.error('Erreur lors de la requête POST :', error);
		});
    setToAdd(!toAdd);
	}

  function previous(){
    const current = new Date(currentDate);
    current.setDate(current.getDate() - 7);
    setCurrentDate(current);
  }
  function next(){
    const current = new Date(currentDate);
    current.setDate(current.getDate() + 7);
    setCurrentDate(current);
  }
  function changeDate(e){
    console.log(e.target.value);
    const selected = new Date(e.target.value);
    setCurrentDate(selected);
  }
  function formatDate(date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  function reload(){
		setReloadCount(reloadCount+1);
	}

  return (
    <div>
      <h1>Tâches de la semaine</h1>

      <div style={{display: 'flex', alignItems:'center', justifyContent: 'center' }}>
        <button onClick={previous}>Précédent</button>
        <input type='date' name='currentDate' value={formatDate(currentDate)}  onChange={(e) => changeDate(e)} style={{width: '20%'}} />
        <button  onClick={next}>Suivant</button>
      </div>


      <button onClick={() => setToAdd(true)}>Ajouter</button>

      <Popup trigger={toAdd} setTrigger={setToAdd}>
        <form onSubmit={(e) =>handleSubmit(e)}>
			    <div className="form-group">
            <label>Nom</label>
            <input type='text' name='nom' placeholder='Nom' required  />
          </div>
			    <div className="form-group">
            <label>Pour quand est la tâche </label>
            <input type='date' name='date_a_faire' placeholder='Date de réalisation' required />
          </div>
          <button type='submit'>Entrer</button>
        </form>
      </Popup>

      <div>			    
        {taches.map((tache) => (
          <Tache key ={tache.id} tache={tache} reload={reload} />
        ))}
      </div>
    </div>
  )
}



export default Taches;
