import React from 'react';
import Switch from 'react-switch';
import { useState, useEffect } from 'react';
import Popup from '../components/Popup.js';
import Fiche from '../components/Fiche.js';
import '../styles/Form.css'
import '../styles/Fiche.css'
import bouton_modifier from '../assets/bouton_modifier.png'	

function Fiches() {

	const [ajoutFicheBool, setAjoutFicheBool] = useState(false);
	const [reloadCount, setReloadCount] = useState(0);
	const [selectBool, setSelectBool] = useState(false);
	const [fiches, setFiches] = useState([]);

	useEffect(() => {
		fetch("http://localhost:9002/fiches/",{type: "GET"})
		  .then(res => res.json())
		  .then(
			(result) => {
				setFiches(result);
			},
			(error) => {
				console.log('Erreur lors de la requête GET :', error);
			}
		  )
	  }, [reloadCount, ajoutFicheBool, selectBool])
	
	function handleSubmit(e) {
		e.preventDefault();
		const formData = {
			nom: e.target['nom'].value,
			description: e.target['description'].value,
			selectionner: false,
			planter_debut: e.target['planter_debut'].value,
			planter_fin: e.target['planter_fin'].value,
			recolte_debut: e.target['recolte_debut'].value,
			recolte_fin: e.target['recolte_fin'].value,
			parcelle_id: 0,
		};

		fetch('http://localhost:9002/fiche/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(formData),
		})
		.then(res => res.json())
		.then(res => reload())
		.catch(error => {
			console.error('Erreur lors de la requête POST :', error);
		});
		setAjoutFicheBool(!ajoutFicheBool);
		console.log('Form ajout de fiche soumis');
	}

	function handleChange(fiche){
		const updatedFiches = fiches.map((f) =>
			f.id === fiche.id ? { ...f, selectionner: !f.selectionner } : f
	  	);
	
	  	setFiches(updatedFiches);
	
		const formData = {
			nom:fiche.nom,
			description: fiche.description,
			selectionner: !fiche.selectionner,
			planter_debut: fiche.planter_debut,
			planter_fin: fiche.planter_fin,
			recolte_debut: fiche.recolte_debut,
			recolte_fin: fiche.recolte_fin,
			parcelle_id: 0,
		};
		fetch(`http://localhost:9002/fiche/${fiche.id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(formData),
		})
		.then(res => res.json())
		.catch(error => {
			console.error('Erreur lors de la requête PUT :', error);
		});
	}

	function reload(){
		setReloadCount(reloadCount+1);
	}

	return (
    <div>
      <h1>Liste des Fiches </h1>

      <button onClick={() => setAjoutFicheBool(true)}>Ajouter</button>
	  <Popup trigger={ajoutFicheBool} setTrigger={setAjoutFicheBool}>
        <form onSubmit={(e) =>handleSubmit(e)}>
			<div className="form-group">
				<label>Nom</label>
				<input type='text' name='nom' placeholder='Nom' required />
			</div>
			<div className="form-group">
				<label >Description</label>
				<input type='text' name='description' placeholder='Description' required   />
			</div>
			<div className="form-group">
				<label >Début de la période pour planter</label>
				<input type='date' name='planter_debut' placeholder='Début de la période de plantaison' required />
			</div>
			<div className="form-group">
				<label >Fin de la période pour planter</label>
				<input type='date' name='planter_fin' placeholder='Fin de la période de plantaison' required  />
			</div>
			<div className="form-group">
				<label >Début de la période pour récolter</label>
				<input type='date' name='recolte_debut' placeholder='Début de la période de récolte' required />
			</div>
			<div className="form-group">
				<label >Fin de la période pour récolter</label>
				<input type='date' name='recolte_fin' placeholder='Fin de la période de récolte' required />
			</div>
			<button type='submit' className="button">Entrer</button>
        </form>
      </Popup>

	  
      <button onClick={() => setSelectBool(true)}>
        <img src={bouton_modifier} alt='icone modifier' className='icone'  />
        Modifier la selection
      </button>
	  <Popup trigger={selectBool} setTrigger={setSelectBool}>
		<h3> Sélectionnez les plants que vous souhaitez cultiver ! </h3>
		<ul>
			{fiches.map((fiche) => (
				<div key={fiche.id}>
					<li>{fiche.nom}</li>
					<Switch checked={fiche.selectionner} onChange={() => handleChange(fiche)}/>
				</div>	
          ))}
		</ul>
	  </Popup>

      <table>
        <tbody className="fiche-list">
          {fiches.map((fiche) => (
            <tr key={fiche.id}>
			  <Fiche fiche={fiche} reload={reload}/>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default Fiches;



