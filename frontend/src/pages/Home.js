import React from 'react';
import { useState, useEffect } from 'react';

function Home() {
  const [jardin, setJardin] = useState([]);
  const [capteurs, setCapteurs] = useState([]);

  useEffect(() => {
		fetch(`http://localhost:9002/jardin/1`,{type: "GET"})
		  .then(res => res.json())
		  .then(
			(result) => {
				setJardin(result);
			},
			(error) => {
				console.log('Erreur lors de la requête GET jardin :', error);
			}
		  )
		  fetch(`http://localhost:9002/capteurs-releve/`,{type: "GET"})
		  .then(res => res.json())
		  .then(
			(result) => {
				setCapteurs(result);
			},
			(error) => {
				console.log('Erreur lors de la requête GET capteurs :', error);
			}
		  )
		
	  }, [])

  return (
    <div>
      <h1>Votre jardin : {jardin.nom}</h1>
	  <h3>Informations Générales</h3>
      <p>Présentation : {jardin.description}</p>
      <p>Addresse : {jardin.numero_adresse} {jardin.rue}, {jardin.code_postal} {jardin.ville}</p>
	  <br/>
	  <div>
		{capteurs.map((capteur) =>
			<div key={capteur.capteur_id}>
				<h3>Dernier relevé de {capteur.nom}</h3>
				<p> Date du relevé : {capteur.date_releve}</p>
	  			<p> valeur : {capteur.valeur} {capteur.unite} </p>
			</div>
		)}
	  </div>
    </div>
  )
}

export default Home;
