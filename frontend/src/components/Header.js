import React from 'react'
import "../styles/Header.css"
import logo from '../assets/logo.png'
import Navbar from '../components/Navbar';

function Header(){
    return(
        <div className="header">
            <img src={logo} alt='logo' className='header-img'/>
            <Navbar/>
        </div>
    );
}
export default Header;
