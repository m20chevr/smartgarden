import React from 'react'
import {Link} from 'react-router-dom'
import "../styles/Navbar.css"
import { useState } from 'react';


function Navbar() {
    const [currentPage, setCurrentPage] = useState('Accueil');
    return (
    <div className='navbar'>
        <Link to='/' className={currentPage === 'Accueil' ? 'active' : 'navbar-a'} onClick={() => setCurrentPage('Accueil')}> Accueil </Link>
        <Link to='/taches' className={currentPage === 'Taches' ? 'active' : 'navbar-a'} onClick={() => setCurrentPage('Taches')} > Tâches </Link>
        <Link to='/fiches' className={currentPage === 'Fiches' ? 'active' : 'navbar-a'} onClick={() => setCurrentPage('Fiches')}> Fiches </Link>
    </div>
    )
}

export default Navbar
