import React from 'react'
import Popup from '../components/Popup.js';
import { useState } from 'react';
import default_plant from '../assets/default_plant.jpg'
import verified from '../assets/verified.png'
import poubelle from '../assets/poubelle.png'
import '../styles/Fiche.css'


function Fiche({fiche, reload}){
    const [toDelete, setToDelete] = useState(false);
    const [detailBool, setDetailBool] = useState(false);

    function handleDeleteButton(fiche){
        fetch(`http://localhost:9002/fiche/${fiche.id}`, { method: 'DELETE' })
		.then(res => res.json())
        .then(res=> reload())
		.catch(error => {
			console.error('Erreur lors de la requête DELETE :', error);
		});
        setToDelete(false);
	}
   
    return(
            <td className='fiche'>
                <img src={default_plant} alt='bannière fiche de plante' className='fiche-cover ' />
                <div className='ItemList' >
                    <h3>{fiche.nom}</h3>
                    {fiche.selectionner ? 
                        <div className='fiche-img-container'>
                            <img src={verified} alt='plante sélectionnée' className='fiche-icon ' />
                        </div>
                        : null }
                </div>
                
                <p> {fiche.description.substring(0, 30)} ...</p>
                <div className='ItemList'>
                    <button onClick={() => setDetailBool(true)} > Détails </button>
                    <img src={poubelle} alt='Poubelle' className='fiche-icon ' onClick={() => setToDelete(true)}/>
                </div>
                <Popup trigger= {toDelete} setTrigger={setToDelete}>
                    <h3>Etes-vous sûr de vouloir supprimer la tâche : {fiche.nom}</h3>
                    <button onClick={() => handleDeleteButton(fiche)}>Oui</button>
                    <button onClick={() => setToDelete(false)}>Non</button>
                </Popup>


                <Popup trigger={detailBool} setTrigger={setDetailBool}>
                    <h3> Détail : {fiche.nom} </h3> 
                    <ul>
                        <li>Description : {fiche.description}</li>
                        <li> Présent dans le jardin : {fiche.selectionner ? <span>oui</span> : <span>non</span>}</li>    
                        <li> Période pour planter : du {fiche.planter_debut} au {fiche.planter_fin} </li>                  
                        <li> Période pour récolter : du {fiche.recolte_debut} au {fiche.recolte_fin} </li>                  
                    </ul>
                </Popup>
            </td>
    );
}
export default Fiche;


