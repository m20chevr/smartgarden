import React from 'react'
import { useState } from 'react';
import Popup from '../components/Popup';
import poubelle from '../assets/poubelle.png'
import '../styles/Taches.css'

function Tache({tache, reload}){
    const [toDelete, setToDelete] = useState(false);
    const [updateValidation, setUpdateValidation] = useState(0);

    function handleChange() {
        tache.fait = !tache.fait ;

        const formData = {
          nom: tache.nom,
          fait: tache.fait,
          date_creation: tache.date_creation,
          date_fait:tache.date_fait,
          date_a_faire : tache.date_a_faire,
          action_id: tache.action_id,
          fiche_id: tache.fiche_id
        };
        fetch(`http://localhost:9002/tache/${tache.id}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        })
        .then(res => res.json())
        .catch(error => {
          console.error('Erreur lors de la requête PUT :', error);
        });
        console.log('PUT')
        setUpdateValidation(updateValidation+1);
      };

    function deleteConfirmation(){

      fetch(`http://localhost:9002/tache/${tache.id}`, { method: 'DELETE' })
      .then(res => res.json())
      .then(res => reload())
      .catch(error => {
        console.error('Erreur lors de la requête DELETE :', error);
      });
      setToDelete(false);
    }

    return(
            <div className='tache'>
                <div className='checkbox'>
                  <input type="checkbox" checked={tache.fait} onChange={handleChange} />
                  <label > {tache.nom} </label>
                  <img src={poubelle} alt='Poubelle' className='lmj-poubelle' onClick={() => setToDelete(true)}/>
                </div>
                <Popup trigger= {toDelete} setTrigger={setToDelete}>
                    <h3>Etes-vous sûr de vouloir supprimer la tâche : {tache.nom}</h3>
                    <button onClick={deleteConfirmation}>Oui</button>
                    <button onClick={() => setToDelete(false)}>Non</button>
                </Popup>
            </div>
    );
}
export default Tache;


