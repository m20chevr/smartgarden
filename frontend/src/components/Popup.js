import React from "react";
import "../styles/Popup.css"
import close from '../assets/close.png'

function Popup(props) {
    return (props.trigger) ? (
        <div className="popup">
            <div className="popup" onClick={() => props.setTrigger(false)} />
            <div className="popup-inner">
                <img src={close} alt='Fermer' className='close-btn' onClick={() => props.setTrigger(false)}/>
                {props.children}
            </div>
        </div>
    ) : null;
}

export default Popup;